<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use App\Repository\AdRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AdRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Ad
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titleAd;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slugAd;

    /**
     * @ORM\Column(type="float")
     */
    private $priceAd;

    /**
     * @ORM\Column(type="text")
     */
    private $intoductionAd;

    /**
     * @ORM\Column(type="text")
     */
    private $contentAd;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $coverImageAd;

    /**
     * @ORM\Column(type="integer")
     */
    private $roomsAd;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="ad", orphanRemoval=true)
     */
    private $images;

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * Allows to initialize slug automatique
     * 
     * @ORM\PrePersist
     * @ORM\PreUpdate
     *
     * @return void
     */
    public function initializeSlug() {
        if (empty($this->slugAd)) {
            $slugify = new Slugify();
            $this->slugAd = $slugify->slugify($this->titleAd);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitleAd(): ?string
    {
        return $this->titleAd;
    }

    public function setTitleAd(string $titleAd): self
    {
        $this->titleAd = $titleAd;

        return $this;
    }

    public function getSlugAd(): ?string
    {
        return $this->slugAd;
    }

    public function setSlugAd(string $slugAd): self
    {
        $this->slugAd = $slugAd;

        return $this;
    }

    public function getPriceAd(): ?float
    {
        return $this->priceAd;
    }

    public function setPriceAd(float $priceAd): self
    {
        $this->priceAd = $priceAd;

        return $this;
    }

    public function getIntoductionAd(): ?string
    {
        return $this->intoductionAd;
    }

    public function setIntoductionAd(string $intoductionAd): self
    {
        $this->intoductionAd = $intoductionAd;

        return $this;
    }

    public function getContentAd(): ?string
    {
        return $this->contentAd;
    }

    public function setContentAd(string $contentAd): self
    {
        $this->contentAd = $contentAd;

        return $this;
    }

    public function getCoverImageAd(): ?string
    {
        return $this->coverImageAd;
    }

    public function setCoverImageAd(string $coverImageAd): self
    {
        $this->coverImageAd = $coverImageAd;

        return $this;
    }

    public function getRoomsAd(): ?int
    {
        return $this->roomsAd;
    }

    public function setRoomsAd(int $roomsAd): self
    {
        $this->roomsAd = $roomsAd;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setAd($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getAd() === $this) {
                $image->setAd(null);
            }
        }

        return $this;
    }
}
