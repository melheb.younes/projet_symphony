<?php

namespace App\Form;

use App\Entity\Ad;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class AnnonceType extends AbstractType
{
    /**
     * Allows to add a label & placeholder to our form 
     *
     * @param string $label
     * @param string $placeholder
     * @return array
     */
    private function getConfig($label, $placeholder ){
        return [
            'label' => $label,
            'attr' => [
                'placeholder' => $placeholder
            ]
        ];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titleAd', TextType::class, $this->getConfig('Titre','Tapez un titre de votre annonce'))
            ->add('coverImageAd', UrlType::class, $this->getConfig("URL de l'image","Donnez l'url de l'image de coverture"))
            ->add('intoductionAd', TextType::class, $this->getConfig('Introduction',"Donnez une description globale de l'annonce"))
            ->add('contentAd', TextareaType::class, $this->getConfig('Description','Tapez une description bien détaillée'))
            ->add('roomsAd', IntegerType::class, $this->getConfig('Nombre de chambres','Nombre de chambres disponible'))
            ->add('priceAd', MoneyType::class, $this->getConfig('Prix par nuit','Indiquer le prix pour une'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ad::class,
        ]);
    }
}
