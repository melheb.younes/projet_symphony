<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Form\AnnonceType;
use App\Repository\AdRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdController extends AbstractController
{
    /**
     * @Route("/ads", name="ads_index")
     */
    public function index(AdRepository $repo) {
        $ads = $repo->findAll();

        return $this->render('ad/index.html.twig', [
            'ads' => $ads
        ]);
    }
    
    /**
     * Create new ad 
     *
     * @Route("/ads/new", name="ads_creat")
     * @return Response
     */
    public function create(){
        $ad = new Ad();

        $form = $this->createForm(AnnonceType::class, $ad);

        return $this->render("ad/new.html.twig", [
            'form'=> $form->createView()
        ]);
    }
    
    /**
     * Show one ad
     *
     * @Route("/ads/{slug}", name="ads_show")
     * 
     * @param [type] $slug
     * @param AdRepository $repo
     * @return Response
     */
    public function show($slug, AdRepository $repo) {
        $ad = $repo->findOneByslugAd($slug);

        return $this->render('ad/show.html.twig', [
            'ad' => $ad
        ]);
    }
}
